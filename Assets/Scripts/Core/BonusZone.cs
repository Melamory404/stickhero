﻿using Assets;
using UnityEngine;

public class BonusZone : MonoBehaviour
{
    #region Fields

    [SerializeField] private GameObject incrementText;

    #endregion

    #region Unity Lifecycle

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag(Constants.PERFECT_TRIGGER_TAG)) return;

        Destroy(collision.gameObject);
        GameObject.FindGameObjectWithTag(Constants.GAME_HANDLER_TAG).GetComponent<GameHandler>().InitPerfect();
        var gameobject = Instantiate(incrementText, collision.transform.position, incrementText.transform.rotation);
        gameobject.transform.parent = gameObject.transform;
    }

    #endregion
}