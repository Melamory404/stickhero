﻿using UnityEngine;

public class DestroyBorder : MonoBehaviour
{
    #region Unity Lifecycle

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(collision.gameObject);
    }

    #endregion
}