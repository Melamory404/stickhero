﻿using System.Collections;
using System.Collections.Generic;
using Assets;
using UnityEngine;

public class PerfectTrigger : MonoBehaviour {


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Constants.PLAYER_TAG))
        {
            Destroy(gameObject);
        }
    }

}
