﻿using Assets;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Fields

    private Animator animator;

    private bool isOnPlatform;
    private bool isOnStick;

    #endregion

    #region Properties

    internal bool IsCanMove
    {
        get { return isOnPlatform || isOnStick; }
    }

    #endregion

    #region Unity Lifecycle

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(Constants.PLATFORM_TAG))
            isOnPlatform = true;
        else if (collision.gameObject.CompareTag(Constants.STICK_TAG)) isOnStick = true;
    }

    public void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(Constants.PLATFORM_TAG))
            isOnPlatform = false;
        else if (collision.gameObject.CompareTag(Constants.STICK_TAG)) isOnStick = false;
    }

    #endregion

    #region Public Methods

    public void Run(bool isRunning)
    {
        animator.SetBool(Constants.PLAYER_RUNNING_STATE_BOOL_PARAMETER, isRunning);
    }

    #endregion
}