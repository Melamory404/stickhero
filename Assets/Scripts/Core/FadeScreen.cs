﻿using System.Collections;
using Assets;
using UnityEngine;
using UnityEngine.UI;

public class FadeScreen : MonoBehaviour
{
    #region Fields

    [SerializeField] private Image fadeImage;

    private bool isFadingOut;

    #endregion

    #region Unity Lifecycle

    private void OnEnable()
    {
        isFadingOut = false;
        StartFadingIn();
    }

    private void Update()
    {
        if (fadeImage.color.a >= Constants.FADE_ALPHA_TOP_BORDER)
            StartFadingOut();
        else if (fadeImage.color.a <= Constants.FADE_ALPHA_LOW_BORDER && isFadingOut) gameObject.SetActive(false);
    }

    #endregion

    #region PrivateMethods

    private void StartFadingIn()
    {
        StartCoroutine(FadeIn());
    }

    private IEnumerator FadeIn()
    {
        for (var i = 0; i < 10; i++)
        {
            fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b,
                fadeImage.color.a + Constants.FADE_DELTA);
            yield return new WaitForEndOfFrame();
        }
    }

    private void StartFadingOut()
    {
        StartCoroutine(FadeOut());
        isFadingOut = true;
    }

    private IEnumerator FadeOut()
    {
        for (var i = 0; i < 10; i++)
        {
            fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b,
                fadeImage.color.a - Constants.FADE_DELTA);
            yield return new WaitForSeconds(Constants.FADE_WAIT);
        }
    }

    #endregion
}