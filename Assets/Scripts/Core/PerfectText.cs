﻿using UnityEngine;

public class PerfectText : MonoBehaviour
{
    #region Event Handlers

    public void OnAnimationEnd()
    {
        gameObject.SetActive(false);
    }

    #endregion
}