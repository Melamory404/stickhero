﻿using UnityEngine;

public class PlusOne : MonoBehaviour
{
    #region Event Handlers

    public void OnAnimationEnd()
    {
        Destroy(gameObject);
    }

    #endregion
}