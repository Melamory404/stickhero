﻿using Assets;
using UnityEngine;

public class DeathBorder : MonoBehaviour
{
    #region Fields

    [SerializeField] private AudioSource deathAudioSource;

    #endregion

    #region Unity Lifecycle

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.CompareTag(Constants.PLAYER_TAG)) return;

        deathAudioSource.Play();
        Destroy(collision.gameObject);
    }

    #endregion
}