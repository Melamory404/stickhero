﻿using Assets;
using UnityEngine;

public class ScoreText : MonoBehaviour
{
    #region Fields

    [SerializeField] private Animator textAnimator;

    #endregion

    #region Event Handlers

    public void OnAnimationEnd()
    {
        textAnimator.SetBool(Constants.CURRENT_SCORE_INIT_BOOL_PARAMETER, false);
    }

    #endregion
}