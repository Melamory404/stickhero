﻿using System.Collections;
using UnityEngine;

public class BackgroundScroll : MonoBehaviour
{
    #region Fields

    [SerializeField] private Material material;

    [SerializeField] private Vector2 offset;

    [SerializeField] private float xVelocity;

    #endregion

    #region Unity Lifecycle

    private void Awake()
    {
        material = GetComponent<Renderer>().material;
    }

    private void Start()
    {
        offset = new Vector2(xVelocity, 0);
    }

    #endregion

    #region Public Methods

    public void StartScroll()
    {
        StartCoroutine(Scroll());
    }

    #endregion

    #region Private Methods

    private IEnumerator Scroll()
    {
        for (var i = 0; i < 3; i++)
        {
            material.mainTextureOffset += offset * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    #endregion
}