﻿namespace Assets
{
    public static class Constants
    {
        public static string PLAYER_TAG = "Player";
        public static string SOUND_PREF = "isSoundEnable";

        public static int ENABLE = 1;
        public static int DISABLE = 2;

        public static float PLATFORM_COLLIDER_DIVIDER = 2f;
        public static float STICK_COLLIDER_DIVIDER = 2f;
        public static float ADDITIONAL_PLAYER_STEP_ON_STICK = 0.1f;
        public static float MAX_LOCAL_SCALE_Y_STICK = 8f;
        public static float ADDITIONAL_SPAWN_DELTA_STICK_X = 0.05f;
        public static float ADDITIONAL_SPAWN_DELTA_STICK_Y = 0.13f;

        public static float STICK_SCALE_INCREMENT = 0.1f;
        public static float STICK_POSITION_INCREMENT = 0.032f;

        public static float STICK_ROTATE_SPEED = 5f;
        public static float QUATERNION_ROTATE_ANGLE = -0.70f;

        public static string BEST_SCORE_PREF = "BestScore";

        public static float PLATFORM_SCALE_LEFT_BORDER = 0.9f;
        public static float PLATFORM_SCALE_RIGHT_BORDER = 2.5f;
        public static float PLATFORM_SPAWN_POSITION_SCALE = 4.5f;

        public static float BONUS_ZONE_SCALE_X = 0.15f;
        public static float BONUS_ZONE_SCALE_Y = 0.05f;

        public static string CURRENT_SCORE_INIT_BOOL_PARAMETER = "isInit";

        public static int CAMERA_BOUNDS_MULTIPLIER = 2;

        public static string PLAYER_RUNNING_STATE_BOOL_PARAMETER = "isRunning";

        public static string PLATFORM_TAG = "Platform";
        public static string STICK_TAG = "Stick";
        public static string BONUS_ZONE_TAG = "Bonus";
        public static string GAME_HANDLER_TAG = "GameHandler";
        public static string PERFECT_TRIGGER_TAG = "PerfectTrigger";

        public static float FADE_ALPHA_LOW_BORDER = 0.05f;
        public static float FADE_ALPHA_TOP_BORDER = 0.95f;
        public static float FADE_DELTA = 0.1f;
        public static float FADE_WAIT = 0.03f;

        public static float PERFECT_TRIGGER_OFFSET = 0.05f;
    }
}