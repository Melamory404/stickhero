﻿using Assets;
using UnityEngine;
using UnityEngine.UI;

public class SoundHandler : MonoBehaviour
{
    #region Fields

    [Header("Audio Sources")]
    [SerializeField]
    private AudioSource[] audioSources;

    [Header("UI Sprites")]
    [SerializeField]
    private Sprite soundOn;

    [SerializeField] private Sprite soundOff;

    [Header("UI Objects")]
    [SerializeField]
    private Button soundButton;

    #endregion

    #region Unity Lifecycle

    private void Start()
    {
        HandlePlayerPrefs();
    }

    #endregion

    #region Private Methods

    private void HandlePlayerPrefs()
    {
        if (!PlayerPrefs.HasKey(Constants.SOUND_PREF)) return;
        var isSound = PlayerPrefs.GetInt(Constants.SOUND_PREF) == Constants.ENABLE;
        if (isSound) return;

        soundButton.image.sprite = soundOff;

        MuteSound();
    }

    private void MuteSound()
    {
        foreach (var audioSource in audioSources) audioSource.mute = !audioSource.mute;
    }

    #endregion

    #region Event Handlers

    public void SoundButton_OnClick()
    {
        if (PlayerPrefs.HasKey(Constants.SOUND_PREF))
        {
            var isSound = PlayerPrefs.GetInt(Constants.SOUND_PREF) == Constants.ENABLE;

            if (isSound)
            {
                PlayerPrefs.SetInt(Constants.SOUND_PREF, Constants.DISABLE);
                soundButton.image.sprite = soundOff;
                MuteSound();
            }
            else
            {
                PlayerPrefs.SetInt(Constants.SOUND_PREF, Constants.ENABLE);
                soundButton.image.sprite = soundOn;
                MuteSound();
            }
        }
        else
        {
            PlayerPrefs.SetInt(Constants.SOUND_PREF, Constants.DISABLE);
            soundButton.image.sprite = soundOff;
            MuteSound();
        }
    }

    #endregion
}