﻿using Assets;
using Assets.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameHandler : MonoBehaviour
{
    #region Fields

    [Header("Game Objects")] [SerializeField]
    private GameObject startPlatform;

    [SerializeField] private GameObject platform;

    [SerializeField] private GameObject playerObject;

    [SerializeField] private GameObject bonusZone;

    [SerializeField] private GameObject bonusTrigger;

    [SerializeField] private GameObject stick;

    [SerializeField] private GameObject destroyBorder;

    [Space(1f)] [Header("UI Objects")] [SerializeField]
    private GameObject startUI;

    [SerializeField] private GameObject gameUI;

    [SerializeField] private GameObject restartUI;

    [Space(5f)] [SerializeField] private BackgroundScroll bgScroll;

    [Space(5f)] [SerializeField] private TextMeshProUGUI currentScoreText;

    [SerializeField] private Animator currentScoreTextAnimator;

    [SerializeField] private TextMeshProUGUI restartScoreText;

    [SerializeField] private TextMeshProUGUI bestScoreText;

    [SerializeField] private GameObject perfectText;

    [SerializeField] private GameObject fade;

    [Header("Game Variables")] [SerializeField]
    private float speed;

    [Header("Sound Sources")] [SerializeField]
    private AudioSource scoreAudioSource;
    [SerializeField] private AudioSource perfectAudioSource;
    [SerializeField] private AudioSource stickFallAudioSource;
    [SerializeField] private AudioSource stickGrowAudioSource;

    private bool isStart;
    private bool isStarted;
    private bool isSpawned;
    private bool isMoving;
    private bool isFirstPlatform = true;

    private Player player;

    private Bounds mainCameraBounds;

    private GameObject currentPlatform;
    private BoxCollider2D currentPlatformCollider;

    private GameObject nextPlatform;
    private BoxCollider2D nextPlatformCollider;
    private Vector3 nextPlatformPosition;

    private GameObject previousPlatform;
    private GameObject previousStick;

    private GameObject currentStick;
    private BoxCollider2D currentStickCollider;


    private int currentScore;

    #endregion

    #region Unity Lifecycle

    private void Start()
    {
        mainCameraBounds = Camera.main.OrthographicBounds();

        player = GameObject.FindGameObjectWithTag(Constants.PLAYER_TAG).GetComponent<Player>();

        currentPlatform = startPlatform;
        currentPlatformCollider = startPlatform.GetComponent<BoxCollider2D>();
    }

    private void Update()
    {
        if (player == null) PrepareRestartUI();

        if (!player.IsCanMove) return;

        var step = speed * Time.deltaTime;

        if (isStart)
        {
            bgScroll.StartScroll();

            Vector3 newPlayerPosition;
            var newPlatformPosition =
                new Vector3(
                    -mainCameraBounds.extents.x +
                    currentPlatformCollider.bounds.size.x / Constants.PLATFORM_COLLIDER_DIVIDER,
                    currentPlatform.transform.position.y);
            if (isStarted)
            {
                newPlayerPosition =
                    new Vector3(
                        currentStickCollider.transform.position.x + currentStickCollider.bounds.extents.x +
                        Constants.ADDITIONAL_PLAYER_STEP_ON_STICK, player.transform.position.y);
            }
            else
            {
                currentPlatform.transform.position = Vector3.MoveTowards(currentPlatform.transform.position,
                    newPlatformPosition, step);
                newPlayerPosition =
                    new Vector3(
                        currentPlatformCollider.transform.position.x + currentPlatformCollider.bounds.extents.x /
                        Constants.PLATFORM_COLLIDER_DIVIDER, player.transform.position.y);
            }


            player.transform.position = Vector3.MoveTowards(player.transform.position, newPlayerPosition, step);


            if (previousPlatform)
            {
                previousPlatform.transform.position = Vector3.MoveTowards(previousPlatform.transform.position,
                    new Vector3(destroyBorder.transform.position.x, previousPlatform.transform.position.y), step);
            }

            if (previousStick)
            {
                previousStick.transform.position = Vector3.MoveTowards(previousStick.transform.position,
                    new Vector3(destroyBorder.transform.position.x, previousStick.transform.position.y), step);
            }

            if (player.transform.position == newPlayerPosition &&
                currentPlatform.transform.position == newPlatformPosition)
            {
                player.Run(false);

                if (nextPlatform)
                {
                    isFirstPlatform = false;

                    previousStick = currentStick;
                    currentStick = null;

                    previousPlatform = currentPlatform;
                    currentPlatform = null;

                    currentPlatform = nextPlatform;
                    currentPlatformCollider = nextPlatformCollider;
                    nextPlatform = null;

                    isStarted = false;
                }
                else
                {
                    if (!isFirstPlatform) AddPoints();

                    isStart = false;
                    isStarted = true;
                    isMoving = false;

                    SpawnPlatform();
                }
            }
        }

        if (nextPlatform)
        {
            nextPlatform.transform.position =
                Vector3.MoveTowards(nextPlatform.transform.position, nextPlatformPosition, step);
        }

        if (currentStick && !isMoving)
            if (currentStick.transform.localScale.y <= Constants.MAX_LOCAL_SCALE_Y_STICK)
            {
                if (!stickGrowAudioSource.isPlaying)
                {
                    stickGrowAudioSource.Play();
                }
                currentStick.transform.localScale = new Vector3(currentStick.transform.localScale.x,
                    currentStick.transform.localScale.y + Constants.STICK_SCALE_INCREMENT);
                currentStick.transform.localPosition = new Vector3(currentStick.transform.position.x,
                    currentStick.transform.position.y + Constants.STICK_POSITION_INCREMENT);
            }

        if (Input.GetMouseButtonDown(0) && isStarted && !isMoving && currentStick == null)
        {
            var stickSpawnPosition = new Vector3(
                currentPlatformCollider.transform.position.x - Constants.ADDITIONAL_SPAWN_DELTA_STICK_X +
                currentPlatformCollider.bounds.size.x / Constants.PLATFORM_COLLIDER_DIVIDER,
                currentPlatformCollider.transform.position.y +
                currentPlatformCollider.bounds.size.y / Constants.PLATFORM_COLLIDER_DIVIDER +
                Constants.ADDITIONAL_SPAWN_DELTA_STICK_Y);
            currentStick = Instantiate(stick, stickSpawnPosition, stick.transform.rotation);
            currentStickCollider = currentStick.GetComponent<BoxCollider2D>();
        }
        else if (Input.GetMouseButtonUp(0) && isStarted && !isMoving)
        {
            stickGrowAudioSource.Stop();
            isSpawned = true;
            isMoving = true;
        }

        if (!isSpawned) return;

        if (currentStick == null)
        {
            isSpawned = false;
            isMoving = false;
        }
        else
        {
            currentStick.transform.RotateAround(
                new Vector3(
                    currentStickCollider.transform.position.x -
                    currentStickCollider.bounds.size.x / Constants.STICK_COLLIDER_DIVIDER,
                    currentStickCollider.transform.position.y -
                    currentStickCollider.bounds.size.y / Constants.STICK_COLLIDER_DIVIDER), Vector3.back,
                Constants.STICK_ROTATE_SPEED);

            if (!(currentStick.transform.rotation.z <= Constants.QUATERNION_ROTATE_ANGLE)) return;

            stickFallAudioSource.Play();
            var bonus = Instantiate(bonusTrigger,
                new Vector3(currentStick.transform.position.x - Constants.PERFECT_TRIGGER_OFFSET + currentStickCollider.bounds.size.x / 2,
                    currentStick.transform.position.y), bonusTrigger.transform.rotation);
            bonus.transform.parent = currentStick.transform;
            isSpawned = false;
            isStart = true;
        }
    }

    #endregion


    #region Public Methods

    public void AddPoints()
    {
        scoreAudioSource.Play();
        currentScoreTextAnimator.SetBool(Constants.CURRENT_SCORE_INIT_BOOL_PARAMETER, true);
        currentScore += 1;
        currentScoreText.SetText(currentScore.ToString());
    }

    public void InitPerfect()
    {
        currentScore += 1;
        currentScoreText.SetText(currentScore.ToString());
        perfectText.SetActive(true);
        perfectAudioSource.Play();
    }

    #endregion


    #region Private Methods

    private void PrepareRestartUI()
    {
        restartUI.SetActive(true);
        gameUI.SetActive(false);

        restartScoreText.SetText(currentScore.ToString());

        if (PlayerPrefs.HasKey(Constants.BEST_SCORE_PREF))
        {
            var currentBestScore = PlayerPrefs.GetInt(Constants.BEST_SCORE_PREF);
            if (currentBestScore < currentScore)
            {
                PlayerPrefs.SetInt(Constants.BEST_SCORE_PREF, currentScore);
                bestScoreText.SetText(currentScore.ToString());
            }
            else
            {
                bestScoreText.SetText(currentBestScore.ToString());
            }
        }
        else
        {
            PlayerPrefs.SetInt(Constants.BEST_SCORE_PREF, currentScore);
            bestScoreText.SetText(currentScore.ToString());
        }
    }

    private void SpawnPlatform()
    {
        var randomScale = Random.Range(Constants.PLATFORM_SCALE_LEFT_BORDER, Constants.PLATFORM_SCALE_RIGHT_BORDER);
        nextPlatform = Instantiate(platform);
        nextPlatform.transform.localScale = new Vector3(randomScale, nextPlatform.transform.localScale.y);
        nextPlatformCollider = nextPlatform.GetComponent<BoxCollider2D>();

        var leftBorder = -mainCameraBounds.extents.x +
                         currentPlatformCollider.bounds.extents.x * Constants.PLATFORM_SPAWN_POSITION_SCALE;
        nextPlatformPosition =
            new Vector3(Random.Range(leftBorder, mainCameraBounds.extents.x - nextPlatformCollider.bounds.extents.x),
                platform.transform.position.y);
        var spawnPlatformPosiition =
            new Vector3(nextPlatformPosition.x + mainCameraBounds.extents.x, nextPlatformPosition.y);

        nextPlatform.transform.position = spawnPlatformPosiition;

        var bonus = Instantiate(bonusZone);
        bonus.transform.parent = nextPlatform.transform;
        bonus.transform.position = new Vector3(spawnPlatformPosiition.x,
            spawnPlatformPosiition.y + nextPlatformCollider.bounds.extents.y);
        bonus.transform.localScale = new Vector3(Constants.BONUS_ZONE_SCALE_X / nextPlatform.transform.localScale.x,
            Constants.BONUS_ZONE_SCALE_Y / nextPlatform.transform.localScale.y, 1);
    }

    #endregion

    #region Event Handlers

    public void PlayButton_OnClick()
    {
        isStart = true;
        startUI.SetActive(false);
        gameUI.SetActive(true);

        player.Run(true);
    }

    public void RestartButton_OnClick()
    {
        fade.SetActive(true);

        isStarted = false;
        isSpawned = false;
        isMoving = false;

        isStart = true;
        isFirstPlatform = true;

        Destroy(currentStick);
        Destroy(previousStick);
        Destroy(previousPlatform);
        Destroy(nextPlatform);

        currentScore = 0;
        currentScoreText.SetText(currentScore.ToString());

        gameUI.SetActive(true);
        restartUI.SetActive(false);

        var newPlayerPosition =
            new Vector3(
                currentPlatformCollider.transform.position.x +
                currentPlatformCollider.bounds.extents.x / Constants.PLATFORM_COLLIDER_DIVIDER,
                playerObject.transform.position.y);
        Instantiate(playerObject, newPlayerPosition, playerObject.transform.rotation);

        player = GameObject.FindGameObjectWithTag(Constants.PLAYER_TAG).GetComponent<Player>();
    }

    public void HomeButton_OnClick()
    {
        SceneManager.LoadScene(0);
    }

    #endregion
}