﻿using UnityEngine;

namespace Assets.Scripts
{
    public static class CameraExtensions
    {
        #region Statics

        public static Bounds OrthographicBounds(this Camera camera)
        {
            var screenAspect = Screen.width / (float) Screen.height;
            var cameraHeight = camera.orthographicSize * Constants.CAMERA_BOUNDS_MULTIPLIER;
            var bounds = new Bounds(
                camera.transform.position,
                new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
            return bounds;
        }

        #endregion
    }
}